## Usage

### Linux

```py
pip3 install numpy # or python3 -m pip
python3 FpTree-Growth.py
```

### Windows

```py
python -m pip install numpy
python FpTree-Growth.py
```